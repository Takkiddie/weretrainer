import sys
sys.path.insert(0, 'character')
from were import *
from recurring import *
from display import *
from displayFile import *

TrainAnswer = 1

class wereStoryEvent(storyEvent):
	def __init__(self):
		self.pro = myWere()
		#Periodic activities
		rec = recurring(self.pro);
		self.events = rec
		#self.pro.description()

#First event, will not act as a normal day, it will just execute
class startstory():
	def __init__(self):
		self.pro = myWere()
	def story(self):
		fileLoad("day0.txt");
		op = disp.showOptions(["Yes","No","Why?"]);
		if op == "Why?":
			fileLoad("day0Why.txt");
			#Set flag "Stacks"
			self.pro.flags[TrainAnswer] = "Why?"
			op = disp.showOptions(["Yes","No"]);
		else:
			self.pro.flags[TrainAnswer] = "noask"
		if op == "Yes":
			fileLoad("day0yes.txt");
			disp.dispPortrate = (happy+".png")
		elif op == "No":
			fileLoad("day0no.txt");
			return "The end."

#Test day.
class genericDay(wereStoryEvent):
	def exeStory(self):
		disp("Super Generic Day! ");
			

#The first day of twenty
class day1(wereStoryEvent):
	def exeStory(self):
		fileLoad("day1.txt")		

#First option
class day2(wereStoryEvent):
	def exeStory(self):
		#Read flag 
		if self.pro.flags[TrainAnswer] == "Why?":
			fileLoad("day2why.txt");
		else:
			fileLoad("day2.txt");

#First mood change
class day3(wereStoryEvent):
	def exeStory(self):
		fileLoad("day3.txt");
		self.pro.mood.changeMood(passive)
		self.pro.mood.changeMood(happy)

#First test
class day4(wereStoryEvent):
	def exeStory(self):
		fileLoad("day4Test.txt");
		#first test of any skill
		if self.pro.testSkll(posturing,0):
			fileLoad("day4passPosturing.txt");
		else:
			fileLoad("day4Fail.txt");
		
		self.pro.unlockActivity(TRUST);
		
		disp("Last event");
		
#Write a function to test skills and display the results.				

