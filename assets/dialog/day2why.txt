(p)blank.png

You hear another knock on the door. You hide Garou in a back room to avoid scaring your guest, and go to answer the door. Even expecting him, you are shocked to see who is at your door.

(p)stacks.png

Mr. Fowl, a pleasure to meet you. Do you mind if I come in, I have a bit of important business that I don't wish to conduct on a porch.

(p)ynervous.png

(I guess Garou was telling the truth.)

Is it about your werewolf?

(p)stacks.png

Yes. I understand that you are the best in the business at training weres and I have an offer for you. It may seem like a dangerous one but...

(p)blanke.png

Wait. How did you know that I... Garou! Get out here, I know your in there!

(p)blank.png

Your werewolf sheepishly steps out of hiding. 

(p)nervous.png

He said yes.

(p)stackse.png

That's... 

(p)stacks.png

...

Well... I suppose I picked the right man for the job after all. 

(p)ynervous.png

So, what exactly is going on here?

(p)blank.png

Stacks pulls out a contract and hands it to you.

(p)stacks.png

I wanted to hire you as Garou's personal trainer, I need him to win a few were-dog tournaments before the end of the year. I'll take care of everything financially. Expenses, supplies, equipment, everything you need. As well as a sizable commission.

Also, my lawyers will handle all the paper work. Sign this and you won't ever have to worry...

(p)yhappy.png

Done!

(p)blank.png

You hand back the signed paper to the surprised Mr. Stacks.

(p)stacks.png

I suppose I'll leave you to it then.

(p)blank.png