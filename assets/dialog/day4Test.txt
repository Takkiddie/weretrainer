(p)1Blank.png

Another walk about town. You've noticed the strange and wary looks you get. Oh well, you knew to expect that.

(p)yhappy.png

(I'd better stay positive if I want Garou to behave)

Lovely out, isn't it?

(p)neutral.png

I guess.

(p)1Blank.png

Even so, you make sure to give Garou plenty of room to avoid passers-by as you go. 

(p)ynervous.png

He won't really let me train his temperament yet, so it's probably best if I keep a distance anyway.

(p)1Blank.png

Suddenly someone starts walking toward you. Some colourfully dressed girl texting with one hand, and holding a leash in the other.

A tiny yippy were-pomeranian is spazzing out on the end of his leash as they make their way down the street.

The wild creature spots your werewolf and starts tugging the girl in your direction. 

The pair gets closer and the woman doesn't look up from her phone to see the coming trouble. You do your best to pull Garou back.

(FailTag) Obedience

Garou doesn't follow your queue yanks back on the leash, which almost pulls you off your feet.