from were import *

#Pure Virtual Class, decides behaviour only.
#Never to be implemented
class lessonEvent(baseEvent):
	def __init__(self,protag):
		self.pro = protag
		#These should all be set in subInit
		self.lesson = None
		self.category = None
		self.goodmoods = None
		self.badmoods = None
		#Error checking, check if are none
		#Set the data of the skill and lesson
		self.subInit()
		self.isFullyInitialized()
		#Add Skill to protagonist
		self.pro.addSkill(self.lesson,self.category,self.goodmoods,self.badmoods)
	#subInit must assign a name, category, good training moods and bad ones.
	#It will also set 
	def isFullyInitialized(self):
		if(self.lesson == None or self.category == None or self.goodmoods == None or self.badmoods == None):
			raise NotImplementedError(type(self).__name__+"not fully initialized");
	def subInit(self):
		raise NotImplementedError("implement subInit");
	def displayData(self):
		return str(self.pro.skills)
	def exeStory(self):
		self.pro.applyLesson(self.lesson)
		#Scaforlding to show level of trained skill
		print(self.pro.skills[self.lesson])
		return self.learned+": "+str(self.pro.skills[self.lesson])+" "

