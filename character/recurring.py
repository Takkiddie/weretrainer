from freetime import *
from display import *
from dailyActivities import *



#Executes every turn.
class recurring(repeatedEvent):
	def __init__(self,protag):
		self.pro = protag;
		#Move this big declaration into 'Freetime.py' Somehow.
		self.freeTime = {}
		self.freeTime[PLAYFETCH] = playFetch(self.pro);
		self.freeTime[MASSAGE] = massage(self.pro);
		self.freeTime[BATH] = bath(self.pro);
		self.freeTime[PARK] = park(self.pro);
		self.freeTime[WATCHPERFORMANCE] = watchPerformance(self.pro);
		self.freeTime[WATCHNEWS] = news(self.pro);
		self.freeTime[WATCHOPPONENTS] = opponents(self.pro);
		self.freeTime[TRUST] = TrustExercise(self.pro);
		#Returns a dictionary of objects and string keys for lessons
		self.lesson = Lessons.getLessons(self.pro)
		#Locking up activities
		#To unlock obediance training (In story)
		self.pro.lockActivity(TRUST);
		#Obedience Training
		self.pro.lockActivity(submission);
		self.pro.lockActivity(patience);
		self.pro.lockActivity(temperament);
	def removeLocked(self,activities):
		ret = [];
		for activity in activities:
			if not (activity in self.pro.lockedActivities):
				ret.append(activity)
		return ret;
	#maybe refactor?
	def play(self):
		menu = playMenu()
		#remove locked activities
		activities = self.removeLocked(self.freeTime.keys());
		activity = menu.doActivity(activities)
		if(activity in self.freeTime):
			disp(self.freeTime[activity].exeStory())
		else:
			disp("not an activity: "+activity);
		disp.dispPortrate = str(self.pro.mood.getMood()) + ".png"
	def train(self):
		menu = trainMenu()
		#remove locked activities
		lessons = self.removeLocked(self.lesson.keys());

		lesson = menu.doActivity(lessons)
		#Also do one for skills
		#lesson = input("Pick a Skill To train: ")
		if(lesson in self.lesson):
			disp(self.lesson[lesson].exeStory())
		else:
			disp("not a lesson: "+lesson);		
