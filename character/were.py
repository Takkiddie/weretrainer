from posture import *
from event import *

class myWere(protagonist):
	_instance = None
	def __new__(cls, *args, **kwargs):
		if not cls._instance:
			cls._instance = super(myWere, cls).__new__(cls, *args, **kwargs)
		return cls._instance
	def __init__(self,name = "Garou"):
		self.name = name
		#Multiplier for skill mastery
		self.aptitude = 5
		self.lessons = {}
		self.lockedActivities = [];
		self.flags = {}
		self.initMoods()
	def lockActivity(self,activity):
		self.lockedActivities.append(activity)
	def unlockActivity(self,activity):
		self.lockedActivities.remove(activity)
	def isLocked(self,activity):
		return activity in self.lockedActivities
	def testSkll(self,skilname,skillLevel):
		return self.skills[skilname].mastery >= skillLevel
	def initMoods(self):
		#Initializing moods
		self.mood.addMood(nervous,excited,0)			#Relaxed, Aggresive
		self.mood.addMood(sad,happy,0)					#passive, Dominant
		self.mood.addMood(passive,dominant,2)			#nervous, excited	
		self.mood.addMood(relaxed,aggressive,1)			#sad, happy
		#The hardest two moods to get into should be:	
	def addSkill(self,name,catagory,G,B):
		self.skills.addSkill(name,catagory,G,B)
	def description(self):
		pos = poses(str(self.mood))
		ret = ""
		ret += "At full height, your werewolf "+self.name+" is seven feet tall. "+pos.d1()+", two triangular ears "+pos.d2()+" A Shaggy brown main covers broad shoulders, running down his back into a long unkempt tail. Another, much shorter layer of grey fur covers the brute's muscular chest, thick arms and powerful digitigrade legs. The wolf's front paws, though shaped like human hands, boast ten dull claws, cracked and chipped by poor care. Like a wolf's, his jaws boast many white sharp teeth. "+pos.d3() 
		ret += "\n"
		ret += "\n"
		ret +=  pos.d4()+" "+pos.d5()+" "+pos.d6()+" a cheap rope, from the corner of the room. He wears a plain collar."
		ret += "\n"
		ret += "\n"
		ret += "He seems fairly "+str(self.mood)
		return ret


