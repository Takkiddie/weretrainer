from were import *
from teachskill import *

#Skill name global constants
#Data classes, they only set data.
#Global 'constants' for skills and categories
#Athletics
athletics = "athletics"
strength = "strength"
class teachStrength(lessonEvent):
	def subInit(self):
		#Skill initialization
		self.lesson = strength
		self.category = athletics
		self.goodmoods = [aggressive]
		self.badmoods = [relaxed]
		self.learned = "You have "+self.pro.name+" lift weights. "
		self.learned += "He's already quite strong, so you teach him proper technique."	

constitution = "constitution"
class teachConstitution(lessonEvent):
	def subInit(self):
		self.lesson = constitution
		self.category = athletics
		self.goodmoods = [relaxed]
		self.badmoods = [aggressive]
		self.learned = "You have "+self.pro.name+" jog to build his stamina."

speed = "speed"
class teachSpeed(lessonEvent):
	def subInit(self):
		self.lesson = speed
		self.category = athletics
		self.goodmoods = [happy,excited]
		self.badmoods = [sad]
		self.learned = "You play games with "+self.pro.name+" to hone his reflexes."	

#Obedience Section
obedience = "obedience"
submission = "submission"
class teachSubmission(lessonEvent):
	def subInit(self):
		self.lesson = submission
		self.category = obedience
		self.goodmoods = [passive]
		self.badmoods = [dominant,aggressive]
		self.learned = "You have "+self.pro.name+" lay down, roll over, and do other very basic, tricks. "	
		self.learned += "There's some arguing before he swallows his pride and listens."
		self.learned += "You finally get him to, grudgingly, lay down."

patience = "patience"
class teachPatience(lessonEvent):
	def subInit(self):
		self.lesson = patience
		self.category = obedience
		self.goodmoods =[relaxed]
		self.badmoods = [nervous,excited]
		self.learned = "You have "+self.pro.name+" perform menial frustrating tasks. "	
		self.learned += "Each time he loses his temper he has to start over."

temperament = "temperament"
class teachTemperament(lessonEvent):
	def subInit(self):
		self.lesson = temperament
		self.category = obedience
		self.goodmoods = [happy]
		self.badmoods = [sad]
		self.learned = "You have "+self.pro.name+" play temperament games. "	
		self.learned += "This will help him follow complex instructions."

#Performance Section
performance = "performance"
posturing = "posturing"
class teachPosturing(lessonEvent):
	def subInit(self):
		self.lesson = posturing
		self.category = performance
		self.goodmoods = [dominant,nervous]
		self.badmoods = [relaxed,sad]
		self.learned = "You have "+self.pro.name+" practice looking scary at a mirror. "	
		self.learned += "You teach him exactly what gestures to use to appear just threatening enough."
dignity = "dignity"
class teachDignity(lessonEvent):
	def subInit(self):
		self.lesson = dignity
		self.category = performance
		self.goodmoods = [passive,sad]
		self.badmoods = [dominant,nervous]
		self.learned = "You have "+self.pro.name+" practice looking refined. "	
		self.learned += "Mostly it's about posture and voice. "
grace = "grace"
class teachGrace(lessonEvent):
	def subInit(self):
		self.lesson = grace
		self.category = performance
		self.goodmoods = [relaxed]
		self.badmoods = [nervous,excited]
		self.learned = "You have "+self.pro.name+" practice moving with grace. "	
		self.learned += "Balance and posture are key."
#Initializes all of the skills, and sends the list of objects to the caller
class Lessons():
	def getLessons(protag):
		lessons = {}
		lessons[strength] = teachStrength(protag);
		lessons[constitution] = teachConstitution(protag);
		lessons[speed] = teachSpeed(protag);
		lessons[posturing] = teachPosturing(protag);
		lessons[dignity] = teachDignity(protag);
		lessons[grace] = teachGrace(protag);
		#Obediance, locked by default
		lessons[submission] = teachSubmission(protag);
		lessons[patience] = teachPatience(protag);
		lessons[temperament] = teachTemperament(protag); #TEMPERAMENT Temperament
		return lessons